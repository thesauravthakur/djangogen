import os
APP_NAME = input('Enter the app name:').lower()
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
APP_DIR = os.path.join(BASE_DIR, f'{APP_NAME}')
HOME_DIR = os.path.join(BASE_DIR, 'home')


def add_app_name_to_home_setting_base():
    setting_base_path = os.path.join(HOME_DIR, 'settings/base.py')
    if os.path.exists(setting_base_path):
        print('found')
        base_file = open(setting_base_path, 'r')
        base_file_allLine = base_file.readlines()
        # print(base_file_allLine)
        for i in range(len(base_file_allLine)):
            if 'INSTALLED_APPS = [\n' == base_file_allLine[i]:
                print('found')
                base_file_allLine[i - 1] = "\nINSTALLED_APPS = [\n"
                base_file_allLine[i] = "\t'saurav',\n"
                base_file = open(setting_base_path, 'w')
                base_file.writelines(base_file_allLine)
                print('test')
                base_file.close()


def urls_file_create():
    urls_file_path = os.path.join(APP_DIR, 'urls.py')
    if os.path.exists(urls_file_path):
        os.remove(urls_file_path)
    views_file_path = os.path.join(APP_DIR, 'views.py')
    views_file_open = open(views_file_path, 'r')
    urls_file_open = open(urls_file_path, 'at')
    views_file_data = views_file_open.read()
    urls_file_open.write('from rest_framework.routers import DefaultRouter\n')
    views_file_data_list = views_file_data.split(' ')
    for words in views_file_data_list:
        if "(viewsets.ModelViewSet)" in words:
            words_list = words.split('(')
            views = words_list[0]
            views = views.split(" ")
            for view in views:
                urls_file_open.write(f'from .views import {view}\n')
    urls_file_open.write('\nrouter = DefaultRouter()\n')
    for words in views_file_data_list:
        if "(viewsets.ModelViewSet)" in words:
            words_list = words.split('(')
            views = words_list[0]
            views = views.split(" ")
            for view in views:
                basename = view.split('V')[0].lower()
                urls_file_open.write(
                    f"router.register(r'', {view}, basename='{basename}')\n")
    urls_file_open.write('urlpatterns = router.urls')
    urls_file_open.close()
    views_file_open.close()


def views_file_create():
    views_file_path = os.path.join(APP_DIR, 'views.py')
    if os.path.exists(views_file_path):
        os.remove(views_file_path)
    serializers_file_path = os.path.join(APP_DIR, 'serializers.py')
    views_file_open = open(views_file_path, 'at')
    views_file_open.write('from rest_framework import viewsets\n')
    serializers_file_open = open(serializers_file_path, 'r')
    serializers_file_data = serializers_file_open.read()
    serializers_file_data_list = serializers_file_data.split(' ')
    for words in serializers_file_data_list:
        if "(serializers.ModelSerializer)" in words:
            words_list = words.split('(')
            serializers = words_list[0]
            serializers = serializers.split(" ")
            for serializer in serializers:
                views_file_open.write(
                    f'from .serializers import {serializer}\n')
    model_file = os.path.join(APP_DIR, 'models.py')
    model_file_open = open(model_file, 'r')
    model_file_data = model_file_open.read()
    model_file_data_list = model_file_data.split(' ')
    models_list = []
    serializers_list = []
    for words in model_file_data_list:
        if "(models.Model)" in words:
            words_list = words.split('(')
            models = words_list[0]
            models = models.split(" ")
            for model in models:
                views_file_open.write(f'from .models import {model}\n')
                models_list.append(model)
    for words in serializers_file_data_list:
        if "(serializers.ModelSerializer)" in words:
            words_list = words.split('(')
            serializers = words_list[0]
            serializers = serializers.split(" ")
            for serializer in serializers:
                serializers_list.append(serializer)
    for index in range(len(serializers_list)):
        serializer_data = serializers_list[index]
        models_data = models_list[index]
        views_file_open.write(
            f'\n\nclass {models_data}ViewSet(viewsets.ModelViewSet):\n\tqueryset = {models_data}.objects.all()\n\tserializer_class = {serializer_data}\n'
        )
    serializers_file_open.close()
    views_file_open.close()
    model_file_open.close()


def serializers_file_create():
    serializers_file_path = os.path.join(APP_DIR, 'serializers.py')
    if os.path.exists(serializers_file_path):
        os.remove(serializers_file_path)
    serializers_file_open = open(serializers_file_path, 'at')
    serializers_file_open.write("from rest_framework import serializers\n")
    model_file = os.path.join(APP_DIR, 'models.py')
    model_file_open = open(model_file, 'r')
    model_file_data = model_file_open.read()
    model_file_data_list = model_file_data.split(' ')
    for words in model_file_data_list:
        if "(models.Model)" in words:
            words_list = words.split('(')
            models = words_list[0]
            models = models.split(" ")
            for modul in models:
                serializers_file_open.write(f"from .models import {modul}\n")
    serializers_file_open.write('\n')
    for words in model_file_data_list:
        if "(models.Model)" in words:
            words_list = words.split('(')
            models = words_list[0]
            models = models.split(" ")
            for modul in models:
                serializers_file_open.write(
                    f"\nclass {modul}Serializers(serializers.ModelSerializer):\n\tclass Meta:\n\t\tmodel = {modul}\n\t\tfields = '__all__'\n\n"
                )
    model_file_open.close()
    serializers_file_open.close()


def tests_file_create():
    tests_file_path = os.path.join(APP_DIR, 'tests.py')
    if os.path.exists(tests_file_path):
        os.remove(tests_file_path)
    tests_file_open = open(tests_file_path, 'at')
    tests_file_open.write('from django.test import TestCase')
    tests_file_open.close()


def apps_file_create():
    apps_file = os.path.join(APP_DIR, 'apps.py')
    if os.path.exists(apps_file):
        os.remove(apps_file)
    apps_file_open = open(apps_file, 'at')
    apps_Name_upperCase = APP_NAME.capitalize()
    apps_file_data = f"from django.apps import AppConfig\n\n\nclass {apps_Name_upperCase}Config(AppConfig):\n\tname='{APP_NAME}'"
    apps_file_open.write(apps_file_data)
    apps_file_open.close()


def admin_file_create():
    model_file = os.path.join(APP_DIR, 'models.py')
    model_file_open = open(model_file, 'r')
    model_file_data = model_file_open.read()
    admin_file = os.path.join(APP_DIR, 'admin.py')
    if os.path.exists(admin_file):
        os.remove(admin_file)
    if os.path.exists(admin_file):
        admin_file_open = open(admin_file, 'at')
    else:
        admin_file_open = open(admin_file, 'at')
        admin_file_header_data = 'from django.contrib import admin\n'
        admin_file_open.write(admin_file_header_data)
    model_file_data_list = model_file_data.split(' ')
    for words in model_file_data_list:
        if "(models.Model)" in words:
            words_list = words.split('(')
            models = words_list[0]
            models = models.split(" ")
            for modul in models:
                admin_file_open.write(f"from .models import {modul}\n")
    admin_file_open.write('\n\n')
    for words in model_file_data_list:
        if "(models.Model)" in words:
            words_list = words.split('(')
            models = words_list[0]
            models = models.split(" ")
            for modul in models:
                admin_file_open.write(f"admin.site.register({modul})\n")
    model_file_open.close()


def app_create():
    if os.path.exists(APP_DIR):
        print("App exists")
    else:
        os.mkdir(APP_NAME)
        temp_file = os.path.join(APP_DIR, 'models.py')
        init__file = os.path.join(APP_DIR, '__init__.py')
        init__file_open = open(init__file, 'at')
        init__file_open.close()
        temp_file = open(temp_file, 'at')
        temp_file_data = 'from django.db import models'
        temp_file.write(temp_file_data)
        temp_file.close()
        add_app_name_to_home_setting_base()
        print(
            f'Your App is created with Name {APP_NAME}\n Model file is also created\n'
        )


def create_model():
    model_name = input("Enter the model name : ").capitalize()
    create_model = f"\n\n\nclass {model_name}(models.Model):\n\t"
    model_file_name = APP_DIR + "/models.py"
    models_file = open(model_file_name, 'r')
    models_file_allLine = models_file.readlines()
    for item in models_file_allLine:
        if model_name in item:
            print(f'Model Exist with Name {model_name}\n\t')
            models_file.close()
            return model_name, 'old'

    models_file = open(model_file_name, 'at')
    models_file.write(create_model)
    print(f"Models created with Name {model_name}\n")
    models_file.close()
    return model_name, 'new'


def field_append_to_file_function(field_name, output_field, field_type,
                                  model_name, model_history):
    model_file_name = APP_DIR + "/models.py"
    if model_history == 'new':
        models_file = open(model_file_name, 'at')
        models_file.write(output_field)
        print(
            f'{field_type} added with field Name {field_name} in {model_name} Model\n'
        )
        models_file.close()
    elif model_history == 'old':
        create_model = f"class {model_name.capitalize()}(models.Model):\n"
        models_file = open(model_file_name, 'r')
        models_file_allLine = models_file.readlines()
        print(models_file_allLine)
        for i in range(len(models_file_allLine)):
            if create_model == models_file_allLine[i]:
                print('found')
                models_file_allLine[
                    i -
                    1] = f"\nclass {model_name.capitalize()}(models.Model):\n\t"
                models_file_allLine[i] = output_field.split('\t')[0]
                models_file = open(model_file_name, 'w')
                models_file.writelines(models_file_allLine)
                models_file.close()


def fields_function(field_name, field_type, model_name, model_history):
    CharFieldCount = True
    EmailFieldCount = True
    IntegerFieldCount = True
    ImageFieldCount = True
    TextFieldCount = True
    FileFieldCount = True
    ForeignKeyCount = True

    if field_type == 'CharField':
        if CharFieldCount:
            output_field = f"{field_name} = models.{field_type}(max_length=250)\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            CharFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}(max_length=250)\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)

    elif field_type == 'EmailField':
        if EmailFieldCount:
            output_field = f"{field_name} = models.{field_type}(max_length=250)\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            EmailFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}(max_length=250)\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
    elif field_type == 'IntegerField':
        if IntegerFieldCount:
            output_field = f"{field_name} = models.{field_type}()\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            IntegerFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}()\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)

    elif field_type == 'ImageField':
        if ImageFieldCount:
            output_field = f"{field_name} = models.{field_type}(upload_to='{model_name}/pictures', null=True, blank=True)\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            ImageFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}(upload_to='{model_name}/pictures', null=True, blank=True)\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
    elif field_type == 'TextField':
        if TextFieldCount:
            output_field = f"{field_name} = models.{field_type}()\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            TextFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}()\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
    elif field_type == 'FileField':
        if FileFieldCount:
            output_field = f"{field_name} = models.{field_type}(upload_to='{model_name}/files', null=True, blank=True)\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
            CharFieldCount = False
            FileFieldCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}(upload_to='{model_name}/files', null=True, blank=True)\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)
    elif field_type == 'ForeignKey':
        refModel = input('Enter the Referencing Model Name :')
        if ForeignKeyCount:
            output_field = f"{field_name} =models.{field_type}('{refModel.capitalize()}', on_delete=models.DO_NOTHING)\n\t"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)

            ForeignKeyCount = False
        else:
            output_field = f"\t{field_name} = models.{field_type}('{refModel.capitalize()}', on_delete=models.DO_NOTHING)\n"
            field_append_to_file_function(field_name, output_field, field_type,
                                          model_name, model_history)


def model_generator():
    count = True
    model_name, model_history = create_model()
    print(model_history)
    while 1:
        model_fields = input(
            "[1] CharField\n[2] EmailField\n[3] IntegerField\n[4] ImageField\n[5] TextField\n[6] FileField\n[7] ForeignKey\n\n[8] CreateNewModel\n[0] EXIT\n\n=>"
        )
        if model_fields == '1':
            field_name = input("Enter the field name : ")
            filed_type = 'CharField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '2':
            field_name = input("Enter the field name : ")
            filed_type = 'EmailField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '3':
            field_name = input("Enter the field name : ")
            filed_type = 'IntegerField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '4':
            field_name = "image"
            filed_type = 'ImageField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '5':
            field_name = input("Enter the field name : ")
            filed_type = 'TextField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '6':
            field_name = "file"
            filed_type = 'FileField'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '7':
            field_name = input("Enter the field name : ")
            filed_type = 'ForeignKey'
            fields_function(field_name, filed_type, model_name, model_history)
        elif model_fields == '8':
            model_generator()
        elif model_fields == '0':
            admin_file_create()
            apps_file_create()
            tests_file_create()
            serializers_file_create()
            views_file_create()
            urls_file_create()
            exit()

        else:
            print("Your Selection Is Invalid")


def main():
    app_create()
    model_generator()


if __name__ == "__main__":
    main()
