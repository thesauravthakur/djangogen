from django.core.management.base import BaseCommand
import os


class Command(BaseCommand):
    help = 'Rename a Django project'

    def add_arguments(self, parser):
        parser.add_argument('new_project_name',
                            type=str,
                            help='The new Django project name')

    def handle(self, *args, **kwargs):
        new_project_name = kwargs['new_project_name']
        folder_to_rename = input("Enter the existing project name:")
        choice = input("Are you sure the existing project name is " +
                       folder_to_rename + " project" + "(y/n)")
        if choice == 'y' or choice == 'Y':
            new_project_name = input("Enter the new project name:")
            file_to_rename = [
                f'{folder_to_rename}/settings.py',
                f'{folder_to_rename}/wsgi.py', 'manage.py'
            ]

            for f in file_to_rename:
                with open(f, 'r') as file:
                    filedata = file.read()

                filedata = filedata.replace(folder_to_rename, new_project_name)

                with open(f, 'w') as file:
                    file.write(filedata)

            os.rename(folder_to_rename, new_project_name)

            self.stdout.write(
                self.style.SUCCESS('project has been renamed to %s' %
                                   new_project_name))
        else:
            print('Rename programe is terminating')
